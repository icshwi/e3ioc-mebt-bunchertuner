>### This project is obsolete⚠️
>Please refer to:
>- [e3-ioc-ecmc-mebt-buncher-tuner-1](https://gitlab.esss.lu.se/iocs/manual/mebt/e3-ioc-ecmc-mebt-buncher-tuner-1)
>- [e3-ioc-ecmc-mebt-buncher-tuner-2](https://gitlab.esss.lu.se/iocs/manual/mebt/e3-ioc-ecmc-mebt-buncher-tuner-2)
>- [e3-ioc-ecmc-mebt-buncher-tuner-3](https://gitlab.esss.lu.se/iocs/manual/mebt/e3-ioc-ecmc-mebt-buncher-tuner-3)

# e3ioc-mebt-bunchertuner

e3 ioc - EtherCAT Motion Control MEBT Buncher Tuner

## Running the IOC

This IOC doesn't need to be compiled nor installed in a specific directory. Just make sure that the versions of `ecmccfg`, `ecmc` and `stream` (also `EthercatMC` if you want to use it instead of the default ECMC native built in motor record support) in `st.cmd` are installed for the EPICS base and `require` version that you are using.

If your E3 environment is activated, just go to the top directory of this IOC and run `iocsh.bash st.cmd`.

## History

Initially this IOC was located in https://github.com/icshwi/ecmccfg/tree/Julen_Bilbao/examples/ESSB-MobileTuner .

